package info.tkware.muda;
import java.io.File;
import java.util.List;
import java.util.Vector;
import java.util.regex.Pattern;
//import org.apache.commons.cli.Option;

public final class FileProcessor {

	//private String path;
	
	/**
	 * Denotes the type of search for this FileProcessor.
	 * 
	 * <li/>IMAGES_ONLY: Only well-known image file formats will be selected.<br>
	 * <li/>ANY_FILE_FILTERED: Allow almost any file type, excluding dotfiles and OS-specific nonsense like thumbs.db<br>
	 * <li/>ANY_FILE: Allow any file, period. No filtering will be done. Why would you do this?<br>
	 * <li/>OTHER: Use a custom search. Must provide a regular expression string to FileProcessor's constructor as the final argument.<br>
	 *
	 */
	public enum SearchType {
		IMAGES_ONLY,ANY_FILE_FILTERED,ANY_FILE,OTHER
	}
	
	/**
	 * Denotes the parameters for an associated regex
	 * <li/> INCLUDE: Return anything that matches the regex.<br>
	 * <li/> EXCLUDE: Return anything that DOES NOT match the regex.</br>
	 *
	 */
	private enum RegexType {
		INCLUDE, EXCLUDE
	}
	
	//A simple wrapper to bind include/exclude to a regex. Too bad we can't extend Pattern...
	private static final class SearchRegex {
		Pattern compiledRegex;
		RegexType mode;
		SearchRegex(String regex_str, RegexType regex_mode) {
			compiledRegex = Pattern.compile(regex_str); //Validates the regex
			mode = regex_mode;
		}
		RegexType getMode() {
			return mode; 
		}
		String getPattern() {
			return compiledRegex.toString();
		}	
	}
	
	public static List<File> fileList = new Vector<File>();
	
	/**
	 * Search the local file system based on the provided parameters
	 * 
	 * @param path location of a folder
	 * @param search SearchType indicating the scope of search
	 * @param <b>MUST</b> be an empty string unless search is set to OTHER
	 * @return A list (Vector) of java.io.File objects
	 * @see SearchType
	 * 
	 */
	public static List<File> Search(String path, SearchType search, String pattern) {
		//Mutually exclusive options
		if (search == SearchType.OTHER && pattern.isEmpty()) {
			throw new IllegalArgumentException("No pattern provided for OTHER searchtype.");
		} 
		if (search != SearchType.OTHER && !pattern.isEmpty()) {
			throw new IllegalArgumentException("Cannot specify a pattern using a searchtype of anything but OTHER");
		}
		SearchRegex regexp_pattern;
		switch(search) {
		case ANY_FILE:
			regexp_pattern = new SearchRegex(".+", RegexType.INCLUDE);
			break;
		case ANY_FILE_FILTERED:
			regexp_pattern = new SearchRegex("(^\\.+$|^Thumbs\\.db)", RegexType.EXCLUDE);
			break;
		case IMAGES_ONLY:
			regexp_pattern = new SearchRegex("(?i).+\\.(jpg|jpeg|bmp|gif|png)$", RegexType.INCLUDE);
			break;
		case OTHER:
			regexp_pattern = new SearchRegex(pattern, RegexType.INCLUDE);
			break;
		default:
			//The IDE yells at us if we don't have a default case even though the Enum ensures it doesn't matter.
			throw new IllegalArgumentException("Bad RegexType in FileHandler.search - this should never happen.");
		}
		
		File searchpath = new File(path);
	
		//TODO: File has a method that accepts Filter, we can probably massively clean this up
		for(File file:searchpath.listFiles()){
			if (regexp_pattern.getMode() == RegexType.INCLUDE) {
				if (file.getName().matches(regexp_pattern.getPattern())) {
					fileList.add(file.getAbsoluteFile());
				}
			} else if (regexp_pattern.getMode() == RegexType.EXCLUDE) {
				if (!file.getName().matches(regexp_pattern.getPattern())) {
					fileList.add(file.getAbsoluteFile());
				}
			}	
		}
		
		return fileList;		
	}
}
