package info.tkware.muda;

///import org.apache.commons.cli.*;

import info.tkware.muda.FileProcessor.SearchType;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;

public class Main {

	//Assorted constants
	public static final Integer EXIT_SUCCESS = 0;
	public static final Integer EXIT_PROG_FAIL = 1;
	public static final Integer EXIT_USER_FAIL = 2;
	public static String appVer = "v2.0";
	public static String appName = "muda ";
	public static String fullAppName = appName + appVer;
	public static JCommander comm = null;
	public static CommandLineOptions cli = null;

	//And so it begins!
	public static void main(String[] args) throws InterruptedException, IOException {
		System.out.println(fullAppName);
	    try {
	    	cli = new CommandLineOptions();
	    	comm = new JCommander(cli, args);
	    	comm.setProgramName(appName);
	    	} catch(ParameterException p) {
	    		System.out.println(p.getMessage());
	    		System.out.println("Try -h for help.");
	    		System.exit(EXIT_USER_FAIL);
	    	}
	    if (cli.help != null) {
	    	comm.usage();
	    	System.exit(EXIT_SUCCESS);
	    }
	    //if(cli.gui) { new GuiClass(); } else { the stuff below }
	    //TODO this :)
	    List<File> files = FileProcessor.Search(cli.localdir, getSearchType(), "");
	    Poster.Dump(files, cli);
	    System.out.println("Work complete. Hail hotwheels!");
	    System.exit(EXIT_SUCCESS);
	}

	//TODO: This probably should be moved elsewhere
	private static SearchType getSearchType() {
		if(cli.nonimages == null) {
			return SearchType.ANY_FILE_FILTERED;
		} else {
			return SearchType.IMAGES_ONLY;
		}
	}

}
