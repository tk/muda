package info.tkware.muda;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date; //Maybe use log4j to the console instead?
import java.util.List;
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;

import com.mashape.unirest.http.*;
import com.mashape.unirest.http.exceptions.UnirestException;

public class Poster {

	public static String postBody = "";
	public static String postHandler = "post.php";
	public static CommandLineOptions opts;

	/**
	 * Post the given files to an imageboard based on the options given
	 *
	 * @param files A list containing File objects to be posted
	 * @param opts A JCommander-annotated class representing the post options
	 * @throws InterruptedException
	 * @throws IOException
	 */
	public static void Dump(List<File> files, CommandLineOptions opts) throws InterruptedException, IOException {
		Poster.opts = opts;

		System.out.println("Beginning dump. Your post deletion password is: " + opts.password);
		System.out.println("====== " + getCurrentTimestamp() + " ======");

		for(int i = 0; i<= files.size(); i++) {
			boolean retry = true;
			int attempts = 0;
			while(retry == true) {
				if (attempts <= opts.maxfailures) {
					try {
						Post(files.get(i), i, files.size()); //TODO: Multiple file posting
						Thread.sleep(opts.delay * 1000);
						retry = false;
					}
					catch (UnirestException e) {
						System.out.println("Error posting " + files.get(i) + ". Retrying..");
						Thread.sleep(opts.errordelay * 1000 + opts.delay * 1000);
						retry = true;
						attempts++;
					}
				} else {
					System.out.println("Hit max failures!");
					System.exit(Main.EXIT_PROG_FAIL);
				}
			}
		}

		//Gotta clean ourselves up as the last thing we do
		Unirest.shutdown();
		System.out.println("====== " + getCurrentTimestamp() + " ======");
	}





	//Maybe make another version of this that accepts an array for multiposting?
	private static void Post(File file, Integer filepos, Integer totalfiles) throws UnirestException {
		System.out.println("Posting: " + file + " ");
		HttpResponse<String> result = Unirest.post(opts.site + "/" + postHandler)
		.header("accept", "*/*")
		.header("referer", opts.site + "/" + opts.board + "/res/" + opts.thread + ".html")
		.field("thread", opts.thread.toString())
		.field("board", opts.board)
		.field("name", opts.name)
		.field("email",  opts.email)
		.field("subject", opts.subject)
		.field("post", "New Reply")
		.field("body", bodyStringBuilder(opts.count, opts.body, filepos, totalfiles))
		.field("file", file)
		.asString();

		System.out.print(result.getStatus() + " " + result.getStatusText() );

		if (result.getStatus() != 200) {
			handleError(result, file.getName());
		}

	}

	private static boolean isDupeFile(String htmlbody) {
		return htmlbody.indexOf("already exists") > -1;
	}

	/**
	 * Handles various HTTP errors that may come up.
	 * @param result - HttpResponse object from a previous post attempt
	 * @throws UnirestException - if we should try again
	 */
	private static void handleError(HttpResponse<String> result, String file) throws UnirestException{
		//Any number of things could have gone wrong here.
		//Dump the raw HTML so we can investigate.
		if (opts.verbose) { System.out.println(result.getBody()); }
		switch(result.getStatus()){
		case 400: //"Bad request"
			//Duplicate file? TODO: A better way to handle this aside from yelling at the user to remove it.
			if (isDupeFile(result.getBody())) {
				//Get the URL returned by the board
				//TODO: THIS IS COMPLETELY BROKEN. Online tester says it works, the runtime hates it and says no matches.
//				Pattern p = Pattern.compile(".+\"(.+)\">already exists.*");
//				Matcher m = p.matcher(result.getBody());
//				System.out.println("Duplicate file: " + file + " located at " + opts.site + m.group(1) + " - cannot continue.");
				System.out.println("Duplicate file: " + file + ".. cannot continue. Please remove it and try again.");
				System.exit(Main.EXIT_USER_FAIL);
			}
			//Otherwise, something else is up. Retry!
			throw new UnirestException("");
		case 404:
			//Thread's not there. User probably mistyped, but a mod could have nuked it too. Either way, fatal.
			System.out.println("\nThread "+ opts.thread + " does not exist on " + opts.board + " Either you mistyped the thread, or a mod did it for free.");
			System.exit(Main.EXIT_PROG_FAIL);
			break;
		default:
			//Something else? Tell the user and try again. They can bail it out if it's borked.
			throw new UnirestException("Unhandled HTTP return code: " + result.getStatus());
		}
	}

	/**
	 * Returns a string representing a post body based on the provided options
	 * @param useCount - boolean representing whether the total post count should be included in the output
	 * @param bodyText - arbitrary text to be inserted into the post body
	 * @param position - current file position inside of the..
	 * @param files - list of files to be processed
	 * @return A completed post String
	 * @see Poster.dump
	 */
	//TODO: Multiposting
	private static String bodyStringBuilder(boolean useCount, String bodyText, Integer position, Integer totalfiles) {
		String finishedText = "";
		if (!bodyText.isEmpty()) { finishedText += bodyText; }
		if (useCount) {
			String count = "(" + position.toString() + "/" + totalfiles.toString() + ")";
			finishedText += " " + count;
		}
		return finishedText;
	}

	private static String getCurrentTimestamp(){
		return new SimpleDateFormat().format(new Date());
	}


}
