package info.tkware.muda;

//import org.apache.commons.cli.*;
//import java.util.List;
//import java.util.Vector;
import com.beust.jcommander.*;

import java.util.List;
import java.util.ArrayList;
import java.util.regex.Pattern;
import java.util.UUID;

public final class CommandLineOptions {
	
	@Parameter
	public List<String> params = new ArrayList<String>();
	
	@Parameter(names = {"-t", "--thread"}, description = "Thread ID to post in", required=true)
	public Integer thread;
	@Parameter(names = {"-b", "--board"}, description = "Board name (directory) to post", required=true)
	public String board;
	@Parameter(names = {"-d", "--directory"}, description = "Local directory containing files to post", required=true)
	public String localdir;
	@Parameter(names = {"-s", "--site"}, description = "Top level site to post to", validateWith = UrlValidator.class)
	public String site = "https://8ch.net";
	@Parameter(names = {"-r", "--random"}, description = "Post files randomly rather than sequentially")
	public Boolean random = false;
	@Parameter(names = {"-S", "--spoiler"}, description = "Mark uploaded files as spoilers")
	public Boolean spoiler = false;
	@Parameter(names = {"-v", "--verbose"}, description = "Be excessively verbose")
	public Boolean verbose = false;
	@Parameter(names = {"-n", "--nonimage"}, description = "Upload non-image file types")
	public Boolean nonimages = false;
	@Parameter(names = {"-R", "--resume"}, description = "Resume from previous upload position")
	public Integer resume;
	@Parameter(names = {"-f", "--userflag"}, description = "Flag ID to post with")
	public Integer userflag = 0;
	@Parameter(names = {"-p", "--password"}, description = "Post deletion password")
	public String password = UUID.randomUUID().toString().split("-")[0];
	@Parameter(names = {"-a", "--agent"}, description = "Set user agent for communication with the site")
	public String useragent = "Mozilla/5.0 (Gentoo GNU/Linux 4.0; GNU/Emacs 24.5) " + Main.fullAppName;
	@Parameter(names = {"-D", "--delay"}, description = "Delay between post attempts")
	public Integer delay = 20;
	@Parameter(names = {"-E", "--errordelay"}, description = "Additional delay when a posting error is encountered")
	public Integer errordelay = 10;
	@Parameter(names = {"-x", "--maxfailures"}, description = "Number of delays tolerated before giving up")
	public Integer maxfailures = 20;
	@Parameter(names = {"--postbutton"}, description = "Name of the 'new post' button")
	public String postbutton = "New Post";
	@Parameter(names = {"-e", "--email"}, description = "Email address to use on each post")
	public String email = "";
	@Parameter(names = {"-N", "--name"}, description = "Name to use on each post")
	public String name = "";
	@Parameter(names = {"-B", "--body"}, description = "Set content of each post")
	public String body = "";
	@Parameter(names = {"-u", "--subject"}, description ="Set subject of each post")
	public String subject = "";
	@Parameter(names = {"-X", "--maxfiles"}, description = "Files per post")
	public Integer maxfiles = 1;
	@Parameter(names = {"-c", "--count"}, description = "Insert count into message body (this file/total files)")
	public Boolean count = true;
	@Parameter(names = {"-h", "--help"}, description = "Show this help", help=true)
	public Boolean help;
	
	
	private class UrlValidator implements IParameterValidator {
		public void validate(String name, String value) {
			String urlPattern = "/http(s)?:\\/\\/.+\\..+\\//i";
			Pattern.compile(urlPattern); //This validates the pattern - an exception is thrown if it's invalid
			if (!value.matches(urlPattern)) {
				throw new ParameterException("Invalid " + name + ", " + value + " is not in the form http://site.tld");
			}
		}
	}
	
}	