MUDA - Multiple Upload Directory Assistant (alternatively: 無駄)
----------------------------------------------------------------

Muda is a directory dumper program written for 8chan and vichan derivatives.

Given a directory, a thread, and a board, it will dutifully post all files in the specified directory automatically, leaving you free to do other things.

Requirements
------------
* Java 8 - **will not work** on any previous versions, yet.

Limitations & Notes
-------------------
* DEV releases are **very unstable** and probably missing features. The Java version of Muda is very very new.
* Only tested on 8chan. It should work on other vichan boards with similar URL structure.
* Irresponsible use of this script is likely to get you banned. Some sites/boards may not want you dumping. Act accordingly.
* For obvious reasons, muda will not work on boards with a captcha enabled.
* Default timeouts work reasonably well for 8chan and are as low as I can get them without causing a lot of flood detections. Experiment if necessary.
* You will obviously get more timeouts if you particpate in the thread you're dumping into.
* Ensure that the type of files you're dumping are allowed on the board you want to dump on, otherwise you will get strange errors.
* The ruby version isn't being worked on anymore, but is still available on a separate branch.

Instructions
------------
 * Make sure Java is in your local path
 * Run the script with the help option to see full details: `java -jar muda.jar -h`
 * Please report any bugs here :)

Usage
-----
Minimum required: `java -jar muda.jar --thread 123456 --board b --directory /home/anon/images`

Other options are available. Examine the output of `-h` to customize your dump.

License
-------
Copyright 2015 TKWare Enterprises

Licensed under the Apache License, Version 2.0 (the "License");
you may not use Muda except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Name
----
You thought you would find information on the name here, but instead you found me, Dio!
